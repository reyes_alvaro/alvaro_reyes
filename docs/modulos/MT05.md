

###^^Conceptos y Herramientas^^
####<font size="4">^^Impresión 3D^^</font>

- Fabricación ***Sustractiva***^**A**^ & Fabricación ***Aditiva***^**B**^
![](../images/MT05/sus_add.jpg){width=""}  


- Además de _Sustractiva y Aditiva_, en muchos sitios encontré la inclusión de una tercera categoría: **_Fabricación Formativa_** ( moldes).
![](../images/MT05/3manuf.jpg#center){width="500"}  


!!! tip "Revelación!"
    :bulb: A esta altura del módulo, una gran novedad para mí fue conocer que el concepto de **fabricación aditiva** no se limita solo a las comunes impresoras 3D [FDM](https://es.wikipedia.org/wiki/Modelado_por_deposici%C3%B3n_fundida)( modelado por deposición fundida). Existen [**7 grandes familias estándar (ISO)**](https://www.iso.org/obp/ui/#iso:std:iso-astm:52900:dis:ed-2:v1:en) definidas pero además, dentro de ellas, diferentes tipos de procesos, tecnologías y materiales.  

![](../images/MT05/addmantech.jpg){width="450" align="left"}  
Ante la necesidad de profundizar un poco más sobre esto, además del curso en la plataforma [EDU](https://edu.utec.edu.uy/about/) me fue de mucha utilidad para conocer sobre fabricación aditiva y diversos tipos de máquinas utilizadas el sitio: [Engineering Product Design.](https://engineeringproductdesign.com/knowledge-base/additive-manufacturing-processes/)
<br clear="left"/>  

!!! danger "<font size="1">^^**SIGLAS** <font size="2">**SIGLAS** <font size="4">**SIGLAS!**^^ </font></font> </font>"  

En este punto, tuve que hacer un alto para organizar( en mi cabeza) las diferencias sustanciales y sobre todo, ordenar la enorme cantidad de términos que van apareciendo.  

:eye: Hay muchas siglas, pero algunas se refieren al proceso principal de fabricación utilizado, mientras que otras se refieren a subcategorías y/o tecnologías que, también a la vez, pueden ser marcas registradas de las propias empresas( ej: SLS).  
^^Ejemplos del rompecabezas^^: **SLS, SLM, SLA, PBF, FDM, VPP, MEX, DED**, etc. todas tienen que ver con fabricación aditiva.

Algunas refieren a alguno de los 7 procesos principales:  
>
- VPP( Vat PhotoPolymerization)  
- MEX( Material EXtrusion)  
- DED( Directed Energy Deposition)  
- PBF( Powder Bed Fusion)

Otras son diferentes subtipos dentro de algún proceso madre:  
>
- SLS( Selective Laser Sintering&#8594;PBF)  
- SLM( Selective Laser Melting&#8594;PBF)  
- FDM( Fused Deposition Modeling&#8594;MEX)  
- SLA( Stereolithography&#8594;VPP)


Para obtener un panorama global del universo de fab. aditiva me resulto muy útil la siguiente infografía del sitio [Hubs](https://www.hubs.com/), donde se muestran las diferentes ramas de esta tecnología incluyendo también los principales fabricantes y marcas.
<font size="2">(Click en la imágen para acceder al pdf original)</font>
[![](../images/MT05/additive.jpg "pdf")](../images/MT05/additive.pdf)

---

***^^Tecnología en acción^^***( algunos ejemplos y momentos Wow!):

<iframe width="560" height="315" src="https://www.youtube.com/embed/oL7bMhPTtDI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; " allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/pTsL-vcrAGA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

<iframe width="560" height="315" src="https://www.youtube.com/embed/ciCxNgROtm4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

<iframe width="560" height="315" src="https://www.youtube.com/embed/fa7aYCylNWM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

<iframe width="560" height="315" src="https://www.youtube.com/embed/9E5MfBAV_tA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/UWOVvSfSjCM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

<iframe width="560" height="315" src="https://www.youtube.com/embed/STAHy6hTP14" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/w9sXqxccRPM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

---

####<font size="4">^^Escaneo 3D^^</font>

- Básicamente se trata de la tecnología que permite crear _modelos 3D de alta presición_ a partir de los objetos físicos reales.  
Es como el camino inverso a modelar para fabricar algo que aún no existe.  
Una primera gran clasificación: **Estático**( el equipo está fijo) o **Dinámico**( va montado en algún tipo de móvil aéreo o terrestre).  
También se distinguen según el alcance o rango en **Cercano, Medio y Largo**.   

- Principales tipos:

  ![](../images/MT05/fotogrametria.jpg){align=left}
      ***Fotogrametría***: es la más simple ya que se crea el modelo digital a partir de fotos desde diferentes ángulos. Luego se procesan las fotos con software que reconstruye el modelo 3D como [Meshroom by AliceVision](https://alicevision.org/) o [Regard 3D](https://www.regard3d.org/)( open source).<br clear="left"/>
---
  ![](../images/MT05/struct2.png){align=right}  
    ***Escaneado por Luz Estructurada***: se proyecta un patrón de luz sobre el objeto a escanear y luego se analizan las imágenes que genera el patrón sobre el objeto a reconstruir.<br clear="right"/>
---
  ![](../images/MT05/lasertime.png){width="300" align=left}  
    ***Laser time-of-flight***: la reconstrucción digital se obtiene mediante los cálculos de los tiempos de ida y rebote del haz láser.<br clear="left"/>
---
  ![](../images/MT05/lasertriang.png){width="300" align=right}  
    ***Láser por triangulación***: triangulación trigonométrica para capturar con precisión una forma 3D como millones de puntos.<br clear="right"/>
---
  ![](../images/MT05/tomografia.png){width="300" align=left}  
    ***Tomografía computarizada***: es mayormente aplicada en medicina y se basa en una serie de fotos 2D seccionando al objeto mediante Rayos X. Luego se superponen como capas para reconstruir el modelo.<br clear="left"/>  


- ***Principales fuentes que usé para aprender sobre Escaneo 3D:***

    >Curso en [EDU](https://edu.utec.edu.uy/about/) de UTEC.  
    [BitFab.](https://bitfab.io/es/)  
    [EMS.](https://ems-usa.com/3d-knowledge-center/3d-scanning-knowledge-center/types-of-3d-scanners-and-3d-scanning-technologies/)  
    Lab. Virtual 30/11/2021 ( uso de escáner ARTEC en vivo).


!!! warning "A tener en cuenta!"
    En términos generales, con cada tipo de escaneo hay que considerar las condiciones óptimas en relación al medio, ubicación y cómo afecta la luz en el objeto a escanear según la tecnología utilizada.  

---         

###^^Impresión 3D FDM^^  

![](../images/MT05/partesFDM.jpg){width="" align=right}  
Es hora de entrar en acción! y poder seguir aprendiendo sobre esta tecnología pero ahora mediante la fabricación real de un objeto mediante ***Impresión 3D en una impresora FDM***( modelado por deposición fundida).<br clear="right"/>  

:eye:*^^Puntos destacados al modelar para impresión 3D^^*
<font size="3">
>1. Trabajar en **mm**.  
>2. Necesidad de una **MESH** y definir suavidad según la cantidad de polígonos.  
>3. Ajustar **Orientación** de manera que el lado mejor soportado quede hacia abajo.  
>4. Ajustar la **escala o tamaño** antes de exportar el archivo para imprimir.
>5. Se necesita un **mínimo espesor** de pared para imprimir (0.5mm).
>6. El modelo debería ser **solido** (poli superficie cerrada o water-tight/estanco) para que se imprima bien.
>7. Ajustar la geometría para no tener **formas dentro** de otras formas.
>8. La impresión se hace capa sobre capa y no puede hacerse en el **aire**.
>9. Las formas o partes que cuelgan o vuelan deben preverse con **soportes o apoyadas** sobre la cama de la impresora.  
>10. Siempre es bueno hacer una **comprobación del modelo** verificando que no tiene problemas estructurales NURBS (por ejemplo curvas abiertas) y tampoco problemas de MESH abiertas (naked edges/bordes de superficie no conectados con otros).  
</font>

---

####^^Parámetros principales FDM^^  
>
1. **^^Altura de capa^^:** según la resolución de superficie requerida( gran impacto en el tiempo de ejecución).  
![](../images/MT05/capas.jpg){width="300"}
>
>
2. **^^Relleno^^:** se pueden definir densidad( %) y tipo.  
![](../images/MT05/infill2.jpg){width="300"}![](../images/MT05/infill.jpg){width="300"}  
>:eye:^^Dato curioso:^^ _con porcentajes de relleno en un rango de 50 a 95%, el tiempo será mayor que si se imprime con 100%[( fuente).](../images/MT05/relleno.pdf)
>
3. **^^Posición/orientación^^:** inside en la terminación superficial, en los soportes necesarios, en la resistencia frente a cargas mecánicas y también en el tiempo( ej: orientada en Z demora más).  
![](../images/MT05/orient2.jpg){width="200"}![](../images/MT05/resist.jpg){width="300"}  
>  
>
4. **^^Soportes^^:** se determinan los necesarios según geometría y orientación ( regla 45^o^).  
![](../images/MT05/soportes.jpg){width="400"}  
>  
>
5. **^^Material^^:** depende de la aplicación/función del objeto, de la apariencia necesaria, y de la capacidad del equipo( temperatura).  
![](../images/MT05/materiales.jpg){width="500"}  
>  
>
6. **^^Ventilación^^:** dependiendo del material será más o menos importante y necesaria la adecuada ventilación/extracción de humos y contaminantes al imprimir( ver cuadro de materiales arriba).  
![](../images/MT05/vent.jpg){width="300"}   
>  
>  
7. **^^Velocidad^^:** afecta directamente el tiempo de impresión y la calidad de la misma ( encontrar el justo equilibrio es la cuestión!). Depende mucho de la capacidad del equipo y del material a utilizar.  
Tabla orientativa según el material( fuente: [3D Solved.)](https://3dsolved.com/3d-printing-speed-vs-quality/)  
![](../images/MT05/speed.jpg)  

---


*^^Principales formatos de archivos^^*  
<font size="3">
>1. **.STL** es el formato básico estándar ampliamente compatible y aceptado. Puede ser en formato _Binario_(recomendado) o _ASCII_.  
![](../images/MT05/stl.png){width="150"}  
    - No contiene información sobre colores, materiales, ni escala.
    - Archivo de tamaño enorme para resoluciones ultra suaves.
    - Propenso a errores en algunas oportunidades.  
    ---
>2. **.OBJ** es un estándar de código abierto que mejora la calidad respecto al STL.  
![](../images/MT05/obj.jpg){width="150"}
    - Es el formato más simple que admite colores y materiales( en archivo adicional MTL).
    - No brinda información sobre escala.
    - Puede resultar en archivos complejos de volver a trabajar.  
    ---  
>3. **.AMF-3MF** es un formato estándar aunque aún no universalmente compatible como los anteriores.  
![](../images/MT05/amf.jpg){width="100"}
    - Gran preponderancia en la industria por ser específicos para fabricación 3D.
    - Mejora notoriamente la incidencia de errores de geometría del modelo.
    - Incluye mucha información adicional como por ejemplo configuración de la cortadora y estructuras de soportes.
    - Incluyen datos de escala.  

</font>

---  
*^^Reparación de archivos para impresión^^*  

  En caso de ser necesario, es posible _reparar_ los archivos, _editar y corregir_ Meshes con varios programas disponibles.
<font size="3">  
>
 - [MeshMixer](https://www.meshmixer.com/)( free)
 - [MeshLab](https://www.meshlab.net/)( open source)
 - [Netfabb](https://www.autodesk.com/products/netfabb/overview)  
 - [Geomagic](https://www.3dsystems.com/software)  

</font>

---  
*^^De lo digital a lo físico^^*  

![](../images/MT05/slicer.png){width="300" align=right}  
***Slicer*** es el software que hace de _puente_ entre el archivo que recibe del programa CAD( .STL, .OBJ, .AMF) y la impresora 3D.<br clear="right"/>   
Basicamente los Slicers 3D realizan 2 operaciones principales:  
<font size="3">
>
1. Convierten el modelo en una serie de capas finas ( laminar el objeto).
2. Generan una serie de instrucciones( archivo [Código G](https://en.wikipedia.org/wiki/G-code)) que controlarán el funcionamiento de la impresora.  
![](../images/MT05/workflow2.jpg)

También analizan y generan lo necesario para obtener el mejor resultado físico según la tecnología aplicada( ejemplo: Soportes).

</font>

*^^Slicers más conocidos^^*  
<font size="3">
>
- [CURA](https://ultimaker.com/software/ultimaker-cura)- desarrollado por Ultimaker, entre los más utilizados.
- [REPLICATOR G](http://replicat.org/)- open source, basado en entornos Arduino-Processing.
- [SKEINGORGE](https://reprap.org/wiki/Skeinforge)- integra el proyecto RepRap.
- [SLIC3R](https://slic3r.org/)- opensource
- [PRUSA SLICER](https://www.prusa3d.com/page/prusaslicer_424/)- basado en Slic3r.
- [ICESL](https://icesl.loria.fr/)- incluye una versión online.  

Otros específicos para imprimir( integran Slicers):
>
- [REPETIER](https://www.repetier.com/)- plataforma muy completa.
- [PRONTERFACE](https://www.pronterface.com/)- open source
- [OCTOPRINT](https://octoprint.org/)- open source, interface web.

</font>  

---
*^^Sitios con Info/Guías & Tips generales^^*  
<font size="3">
>
- [Prismacim](https://prismacim.com/normas-basicas-de-diseno-para-impresion-3d/)- Normas básicas de diseño para impresión 3D.
- [Formlabs](https://formlabs.com/latam/blog/grosor-minimo-pared-impresion-3d/)- El grosor mínimo de pared para la impresión 3D.
- [Skulp3d](https://skulp3d.com/guia-de-modelado-impresion-en-3d/)- Guía de modelado para Impresión 3D.

</font>  

---
###^^Desafío^^  

Este módulo implica modelar un elemento que sea de utilidad para el proyecto final o para su desarrollo( medidas máx. 60x60x60mm).  
![](../images/MT05/ultim3.png){width="200" align=left}  
La producción del mismo se hará en un equipo FDM de los laboratorios de Utec, en este caso una impresora **Ultimaker 3**.  
El programa slicer a utilizar será [CURA.](https://ultimaker.com/software/ultimaker-cura)  
<br clear="left"/>  


####^^Modelado de la pieza^^  

Buscando un elemento que fuera de utilidad para el proyecto y además estuviera de acuerdo con el requerimiento de medidas del desafío me decidí modelar y fabricar un prototipo de: ^^la pieza de sujeción/unión^^ entre la armadura y el contenedor interior del compostador.  
![](../images/MT05/fijadores2.jpg){width="300"}  

***^^Camino recorrido^^***

1. El modelado lo hice en Fusion360  
  ![](../images/MT05/fusion5.jpg){width="500"}
  ![](../images/MT05/fusion6.jpg){width="500"}
  ![](../images/MT05/fusion7.jpg){width="500"}
  ![](../images/MT05/fusion3.jpg){width="500"}  
  ![](../images/MT05/fusion2.jpg){width="500"}
  ![](../images/MT05/fusion1.jpg){width="500"}
  <iframe src="https://estudiantes478.autodesk360.com/shares/public/SH35dfcQT936092f0e435e4a4c52e1123277?mode=embed" width="560" height="315" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>  


2. Lo exporté como STL(binario)  
  ![](../images/MT05/fusion4.jpg){width="500"}  


3. Abrí, ajusté parámetros y simulé con el modelo en Cura.  
  ![](../images/MT05/cura1.jpg){width="500"}  
  ![](../images/MT05/cura2.jpg){width="500"}
  ![](../images/MT05/cura3.jpg){width="500"}
  ![](../images/MT05/cura4.jpg){width="500"}
  ![](../images/MT05/cura5.jpg){width="500"}
  ![](../images/MT05/cura6.jpg){width="500"}  

4. Exporté el archivo Gcode final para enviarlo junto con el archivo STL al laboratorio de UTEC.  
  ![](../images/MT05/cura10.jpg){width="500"}  
  Descargar carpeta con [archivos fuente.](../images/MT05/desafio3d.zip)

---

####^^La hora de la verdad!^^

^^Extracto(4min.) de clase Laboratorio Virtual( vía Zoom)^^  

<iframe width="560" height="315" src="https://www.youtube.com/embed/qGA6iBSyhs4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  


Al imprimir la pieza el resultado fue bastante bueno, sin grandes complicaciones ya que la pieza era bastante sencilla y no tenía puntos delicados con temas de soportes o terminaciones estéticas requeridas( por su función y ubicación en el proyecto).  
![](../images/MT05/piezareal1.jpg){width="500"}  
![](../images/MT05/piezareal2.jpg){width="500"}  



!!! danger "ERROR de orientación"
    Luego de fabricada la pieza, me di cuenta que la cargas/fuerzas mecánicas que va a soportar se darán de forma perpendicular a la disposición de las capas por lo que seguramente se rompa.  
    La ^^_orientación_^^ debió haber sido 90^o^ respecto a como fue impresa según mis ajustes previos. Con ello las capas quedarían en la dirección que hace a la pieza más resistente y eficiente en su función de fijación y sostén( además de eliminar el problema de soporte comentado en el video arriba del lab. virtual).

***La posición debería haber sido así:***  


![](../images/MT05/cura7.jpg){width="500"}  
![](../images/MT05/cura8.jpg){width="500"}
![](../images/MT05/cura9.jpg){width="500"}  

!!! tip "Comentario final"  
    Módulo intenso y de mucho aprendizaje para mi!!!
