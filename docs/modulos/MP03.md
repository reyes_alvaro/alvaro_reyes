
!!! question "NOVEDAD!"
    Particularmente en este módulo, pude conocer nuevas miradas/enfoques sobre un tema hasta ahora para mi desconocido y muy relacionado a mi [proyecto](../proyecto/proyecto.md). Pude sorprenderme con las posibilidades y el valor que los residuos( cáscaras de huevo por ejemplo!) pueden tener al ser reutilizados, transformados, mezclados, etc...  
    Dándoles nuevas formas para generar nuevos proyectos que mejoren nuestro entorno y contribuyan a preservar el medio ambiente...  
    Lo veo como posible complemento a mi proyecto enmarcado en una lógica imperiosa para el futuro inmediato de **economía circular** y reducción de nuestra huella de carbono...
    Percibo los **BIOMATERIALES:** como un universo con un presente en desarrollo y constante expansión, un lindo camino para explorar personal y colectivamente espero...

---

###^^Materiales y Circularidad^^
Expositora: [Laura Freixas](https://www.linkedin.com/in/laura-freixas-b7240885/?originalSubdomain=es) ( Barcelona).    

Los 3 principios básicos de la economía circular:  
>
- Eliminar(o minimizar) residuos y contaminación.
- Hacer circular los productos y materiales lo máximo posible.
- Regenerar la naturaleza.  

Para lograrlo:
>
- _Se necesita ==***REPENSAR***==_
- _Se necesitan **Diseñadores** con formas nuevas de pensar los materiales y productos_
- _Diseños modulares_
- _Diseñar productos con materiales de fácil reparación_  

Fuente: [Ellen Macarthur Foundation](https://ellenmacarthurfoundation.org/)

<iframe width="560" height="315" src="https://www.youtube.com/embed/zCRKvDyyHmI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<font size="2">^^Transcripción^^: _"Living systems have been around for a few billions years, and will be around for many more. In the living world, there is no landfill: instead, materials flow. One species waste is another’s food, energy is provided by the sun, things grow, and dye, and nutrients return to the soil safely. And it works. Yet as humans, we have adopted a linear approach. We take, we make, and we dispose. A new phone comes out, so we ditch the old one. Our washing machine packs up, so we buy another. Each time we do this, we are eating in a finite supply of resources, and often producing toxic waste. It simply can’t work long-term. So what can? If we accept that the living world cyclical model works, can WE change our way of thinking, so that we too operate a circular economy. Let’s start with the biological cycle: how can our waste build capital rather than reduce it?  By rethinking and redesigning products and components and the packaging they come in, we can create safe and compostable materials that help grow more stuff. As they say in the movies: no ressources have been lost in the making of this material! So what about the washing machines, mobile phones, fridges…? We know they don’t biodegrade. Here, we are talking about another sort of rethinking. A way to cycle valuable metals, polymers and alloys, so they maintain their quality and continue to be usueful beyond the shelf life of individual products. What if the goods of today become the resources of tomorrow? It makes commercial sense: instead of the throw away and replace culture we become used to, we adopt a return and renew one, where products and components are designed to be disassembled and regenerated. One solution maybe to rethink the way we view ownership. What if we actually never own our technologies? We simply licence them from the manufacturers. Now, let’s put the biological cycle and the technical cycle together. Imagine if we could design products to come back to their makers, their technical material being reused, and their biological increasing agricultural value. And imagine that these products are made and transported using renewable energy. Here we have a model that builds prosperity long-term. And the good news is, there are already companies out there who are beginning to adopt this way of working. But a circular economy is not about ONE manufacturer changing ONE product. It is about ALL the interconnecting companies that form our infrastructure and economy - coming together. It is about energy, it is about rethinking the operating system itself. We have a fantastic opportunity to open new perspectives and new horizons. Instead of remaining trapped in the frustrations of the present. With creativity and innovation, we really can rethink and redesign OUR future."_ </font>  

^^Ciclo técnico y ciclo biológico en la economía circular^^  

- En el **ciclo técnico**, los productos y materiales se mantienen en circulación a través de procesos como la reutilización, reparación, re-manufactura y reciclaje.  
- En el **ciclo biológico**, los nutrientes de los materiales biodegradables se devuelven a la Tierra para regenerar la naturaleza.

![](../images/MP03/butterfly.png){width=""}

**Buenas fuentes a explorar:**  
>
- [Materiom](https://materiom.org/): biomateriales con recetas para probar!  
- [Material District](https://materialdistrict.com): plataforma de materiales innovadores.

---
###^^FabTextiles: FD en la Moda^^  
Expositora: [Anastasia Pistofidou](https://www.linkedin.com/in/anastasia-pistofidou-75b17255/?originalSubdomain=es) ( Barcelona).  

^^BioDiseño^^:  
>
- El rol del diseñador debe tener en cuenta desde dónde vienen los materiales o materia prima y hacia dónde y cómo terminarán...
- Colores sintéticos o químicos muy nocivos para el medio ambiente.  
- Colores naturales desde productos como remolacha, naranja, etc. o también desde micro organismos o bacterias (colores biológicos).  
- Ejemplo de materiales para la confección: ^^cuero biológico^^ a partir de te Kombucha.   

**Buenas fuentes a explorar:**
>
- [Bioplastic Cook Book](https://issuu.com/nat_arc/docs/bioplastic_cook_book_3): recetas.
- [Materiability](https://materiability.com/): proyectos y materiales con recetas  
- [Fabriacademy](https://class.textile-academy.org/): sitio con recursos, info, tutoriales.  
- [beGROUNDED](http://class.textile-academy.org/2019/students/lara.campos/projects/Documentation/#introduction): proyecto muy interesante acerca de textiles y plantas by [Lara Campos](https://lara-campos.com/)
- [Biomateriales Fab Lab BCN](https://fablabbcn.org/blog/emergent-ideas/biomaterials-101): recetas y más.
- [Remix el Barrio](https://fablabbcn.org/projects/siscode-remix-el-barrio): proyecto de Diseño con biomateriales de restos de alimentos:<iframe width="560" height="315" src="https://www.youtube.com/embed/vPxGAgWKWMk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

###^^Experimentando con Biomateriales^^  

**Componentes básicos de un biomaterial**  
>
- Biopolímeros: agar agar, gelatinas, resina.
- Plastificantes: glicerina, cera.
- Aditivos: residuos orgánicos aportan la resistencia, color, fibras
- Disolventes: agua (para gelatina y agar agar), alcohol (para resina). Desaparecen por evaporación.


**Tips**  
>
- Carozos: como regla general: hervir + moler + secar
- Según el granulado del aditivo es mejor la gelatina (mayor tamaño) o el agar agar (menor tamaño)
- Moldes: para agar agar: liso (acrílico), para gelatina: rugosa (textil poroso), para resina: desmoldar sobre papel de horno para que evitar que se pegue.
- Se pueden agregar aceites esenciales o vinagre a las mezclas para evitar el crecimiento de hongos.

^^Manos a la obra^^  

![](../images/MP03/pract4.jpg){width=""}
![](../images/MP03/pract1.jpg){width=""}
![](../images/MP03/pract5.jpg){width=""}
![](../images/MP03/pract3.jpg){width=""}
![](../images/MP03/pract2.jpg){width=""}
![](../images/MP03/pract6.jpg){width=""}
![](../images/MP03/pract7.jpg){width=""}
