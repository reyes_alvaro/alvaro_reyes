
![](../images/TIp/grupo.jpg)  

---  
####^^Propuesta^^
La práctica para realizar en equipo fue montar y experimentar con el funcionamiento de una extrusora de pasta.  
Para ello teníamos una impresora Anet A8 que debíamos adaptarle un mecanismo extrusor de pasta (previamente impreso 3D) y experimentar con el software de slice para alcanzar los mejores resultados...  
Unos de los mayores problemas fue determinar y obtener la mejor consistencia de la arcilla o masa a utilizar para imprimir. La presencia de grumos y la densidad no uniforme hacía difícil obtener un funcionamiento adecuado del sistema.  
Acceder al [pdf](./ti.pdf) del programa del taller.

**Algunas imágenes/videos que ilustran lo vivido:**  

![](../images/TIp/montaje.jpg)
![](../images/TIp/masa1.jpg)
![](../images/TIp/masa2.jpg)
![](../images/TIp/mejor.jpg)  
![](../images/TIp/mejor2.jpg)  


<iframe width="560" height="315" src="https://www.youtube.com/embed/S5HWDuSgsGY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

<iframe width="560" height="315" src="https://www.youtube.com/embed/HL-b5sfDORQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

####^^Documento del equipo para la presentación final:^^  




####^^Video Institucional sobre el taller:^^

<iframe width="560" height="315" src="https://www.youtube.com/embed/F8Dq9xU6Ftc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

!!! Tip "Comentarios"
    Sobre el taller presencial (3 días) puedo decir que lo que fundamentalmente aportó fue la posibilidad de integración y generación de vínculos entre estudiantes y docentes locales que realizamos el curso.  
    Creo que en ese aspecto cumplió el objetivo!  
    :eyes:De todos modos, me hubiera gustado haber tenido la posibilidad de experimentar y poner en práctica muchos más aspectos técnicos que veníamos viendo de manera teórica online.    
