
###^^Arrancando...^^

¿ Qué es Markdown, GitLab, Git, MkDocs o Atom?  ¿ Qué es hacer fetch, add, commit, push, etc?  
Términos, nombres y conceptos desconocidos para mi al comenzar este módulo.  

Antes de seguir, una canción que expresa mi sentir al iniciar el camino:  

[![](../images/MT01/duran.jpg){width=500px}](https://www.youtube.com/watch?v=Fbl7hwCgETU)

###^^Aterrizando conceptos^^  

- ^^GitLab:^^ "es un servicio web de control de versiones y desarrollo de software colaborativo basado en Git(protocolo). Además tiene un servicio de alojamiento de wikis. Todo con filosofía open source."  
**Es decir:** _es el sitio que usaré como repositorio(con histórico de cambios) con toda la documentación y con el código que a su vez permitirá verse en una página web personal. Esta forma de trabajo permite tener siempre una copia funcional y otra copia de desarrollo en la que puedo modificar y/o agregar contenido._  

- ^^Markdown:^^ "is a text-to-HTML conversion tool for web writers. Markdown allows you to write using an easy-to-read, easy-to-write plain text format, then convert it to structurally valid XHTML (or HTML)."  
**Es decir:** _es el lenguaje( o mejor pseudolenguaje) utilizado para escribir documentos con texto normal( "formato plano") y poder pasarlo al formato HTML( página web) de manera sencilla y directa._

- ^^MkDocs:^^ "is a static site generator designed for building documentation websites. Written in the Python programming language. Documentation source files are written in Markdown, and configured with a single YAML configuration file( mkdocs.yml). MkDocs is an open-source project."  
**Es decir:** _es un generador de sitios web estáticos. Utiliza como fuente para construir la página archivos escritos en lenguaje Markdown._  

- ^^Atom:^^ "is a free and open-source text and source code editor for macOS, Linux, and Microsoft Windows with support for plug-ins written in JavaScript, and embedded Git Control."  
**Es decir:** _es un editor de texto especial (editor web). Posee características especificas para el lenguaje Markdown y el trabajo integrado con GitLab._  

!!! Abstract "Conclusión"
    Escribo la documentación en ^^_Markdown_^^, utilizando ^^_Atom_^^ como editor y con ^^_MkDocs_^^ se genera la página web. Cada vez que hago un *"push"* se genera una nueva versión en GitLab y se ejecutan comandos que están en el archivo ^^gitlab-ci.yml^^ (***"mkdocs build"*** por ejemplo) que actualizan el contenido de la página web.

---

###^^Fuentes principales^^  
Para aprender sobre documentación, GitLab, creación de esta página web o MkDocs:

- curso **"MT01 - Introducción al diseño web – Herramientas Digitales"** en la plataforma **[EDU](https://edu.utec.edu.uy/about/)**
- sobre **[GitLab.](https://docs.gitlab.com/)**
- sobre **[MkDocs.](https://www.mkdocs.org/)**
- sobre **[Material/MkDocs.](https://squidfunk.github.io/mkdocs-material/)**
- sobre Markdown **[básico](http://markdowntutorial.com/)** o características **[avanzadas](https://alinex.gitlab.io/env/mkdocs/#simple-formats)**.

---
###^^El camino recorrido para crear la página web^^

1. Acceder y crear cuenta en [GitLab](https://gitlab.com/).

    !!! Warning "Advertencia"
        No utilizar el nombre de usuario en GitLab con punto "." ya que se toma como parte de la url de la página y genera un error (experiencia propia!).

        ![](../images/MT01/errorpag1.jpg){width="500"}  
        Buscando el motivo encontré la [limitación](https://docs.gitlab.com/ee/user/project/pages/introduction.html#limitations) de GitLab para los nombres.  
        **Solución:** debí cambiar el usuario a "_reyes_alvaro_"

2. Instalar [Git](https://gitforwindows.org/) en mi PC.
3. Setear Git en la terminal(git bash) con las credenciales de la cuenta creada en GitLab:

    >_git config --global user.name "reyes_alvaro"_  
    >_git config --global email.user"alvaro.reyes@estudiantes.utec.edu.uy"_

4. Generar una SSH Key para establecer una comunicación segura entre mi PC y GitLab.  

    >_ssh-keygen -t rsa -C "alvaro.reyes@estudiantes.utec.edu.uy"_ (en git bash)

5. Copiar la clave generada:

    >_clip < ~/.ssh/id_rsa.pub_  (en git bash)

6. Añadir dicha clave en mi cuenta de GitLab (_Add SSH key_)    

7. Hacer _"Fork"_ (save as) del repositorio [template](https://gitlab.com/utecfablabbcn/mkdocstudent) de MkDocs que voy a usar para documentar.

    !!! Note "Nota"
        Antes estuve investigando, pasando algunas horas viendo y probando alternativas, evaluando pros y contras de usar otra plantilla web de algunos de los sitios gratuitos y decidí que lo mejor era no complicar en aspectos innecesarios para el fin que tiene esta página. [(HTML5 UP)](https://html5up.net/),  [(Start Boostrap)](https://startbootstrap.com/)

8. Ahora tengo los archivos en mi repositorio GitLab y hago la descarga en mi computadora con _"git clone"_ en la terminal (git bash). La dirección la saco de mi cuenta de GitLab (clone with SSH).  
![](../images/MT01/clone.jpg){width=500px}

9. Hacer que los cambios en los archivos locales(hechos en el editor) se validen y suban al repositorio a través de la terminal con comandos con:  _git add_, _git commit_, _git push_.
![](../images/MT01/add.jpg){width="500"} ![](../images/MT01/comm_push.jpg){width="500"}

10. !!! Failure "Error"
      Al llegar a este punto, se detectó un problema en GitLab que a partir de mayo 2021 pide una validación de las cuentas por medio de tarjeta de cédito (requisito para generar la página y publicarla).
      ![](../images/MT01/validar.jpg){: width="500" }

    **_Validé la cuenta sin inconvenientes y de forma inmediata._**

11. Se ejecutó un nuevo _git push_:

    !!! success "Éxito!"
        ![](../images/MT01/pagOK.jpg){: width="500" }     

---
###^^Edición y contenido^^
Una vez de tener creada y operativa la página web, comenzó la etapa de editarla, darle formato, colores, aspecto y contenido personal.
Para ello instalé y comenzé a utilizar **[Atom](https://atom.io/)**

!!! Tip
    Una herramienta que me resultó fundamental para esta etapa y aceleró mi curva de aprendizaje: ^^previsualización^^ de la página localmente [(mkdocs serve).](https://mkdocs-dupe-test.readthedocs.io/en/latest/#installation) <br>De esa manera veo los cambios de forma "inmediata" en un navegador. Solo hago "push" luego de saber cómo realmente se verá, siendo por tanto mucho más eficiente.  
---
