


Sintaxis y recursos Markdown (uso interno)

<!-- {width="700" } para ajustar ancho a la imagen -->
<!-- &nbsp; para dejar un espacio vertical -->

==highlight==

TABLA:
| Tabla       | Detalle                        |
| ----------- | ------------------------------------ |
| `Item1`      | :material-check:     ok  |
| `Item2`      | :material-check-all: ok |
| `Item3`      | :material-close:     mal |

CHECKOK:
:material-check:

DobleCheckOK:
:material-check-all:

CheckError:
 :material-close:

flecha izquierda : ← &#8592;
flecha hacia arriba : ↑ &#8593;
flecha derecha : → &#8594;
flecha hacia abajo : ↓ &#8595;

cambiar tamaño texto:
<font size="2"> This is my text number 2 </font>

nota a pie de pág.:
blabla[^1]
[^1]: blablabla
