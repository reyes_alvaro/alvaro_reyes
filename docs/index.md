



#¡Comienzo del viaje!
Hola, soy [Álvaro Reyes](./about/sobremi.md) de Uruguay y este sitio documenta mi _camino de aprendizaje_ durante la ^^Especialización en Fabricación Digital e Innovación Abierta^^ de [UTEC](https://utec.edu.uy/es/) & [Fab Lab Barcelona.](https://fablabbcn.org/)  
¿ Cómo será ese camino? ¿ Muy sinuoso o empedrado? Solo después de recorrerlo lo sabré...  
Ahí vamos!![](./images/camino.jpg "camino de aprendizaje")
