---
hide:
title: Sobre mi

---

#Cómo llegué hasta aquí


![](../images/yo2.jpg "Valle del Lunarejo(Uruguay)"){ align=left }
Sobre mi podría decir que tengo un título en electrónica pero que **_soy_** más bien un maker de alma sin diploma. De pequeño ya me gustaba trabajar con madera y a los 15 años tuve mi primera máquina combinada. Me define el gusto por el "hacer", pero me **_apasiona_** el proceso creativo que surge de aprender e investigar nuevas cosas.  
A lo largo de los años trabajé en una barraca de hierros, en una empresa de electrónica, tuve mi emprendimiento de juguetes de madera, fabriqué una CNC, y actualmente además de trabajar en mi taller, trabajo en un colegio a cargo de un espacio maker.   
Disfruto de las cosas simples y naturales de mi entorno tanto como de los asombrosos cambios en los que la tecnología nos involucra a diario.
Ojalá que a lo largo de esta especialización pueda aprender a desarrollar un proyecto que integre mis pasiones, valores e intereses, a la vez que pueda aportar algo bueno a otros.  
Para terminar, mi frase de cabecera: **_Buscando siempre, Encontrando a veces..._**
