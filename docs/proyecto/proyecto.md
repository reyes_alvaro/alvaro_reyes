
# Propuesta final   

  <iframe width="560" height="315" src="https://www.youtube.com/embed/G-YsPFwrJ4g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>   

<!-- ![](../images/proyecto/compostasio0.jpg) -->

!!! warning "Propuesta Final vs. Punto de partida?"
    Aunque esta es la última página que estoy completando, debo decir que lo siento como la primera de una nueva historia...  
    Aquí intentaré mostrar el camino y los pasos dados para llegar hasta la materialización del primer prototipo del proyecto que me había planteado desde el comienzo de la especialización...  
    Por las magnitudes mínimas reales que **Compostasio** requiere, el planteo para esta etapa fue poder fabricar un modelo a escala 1:3.  
    Sin embargo, el objetivo será utilizar la mayor cantidad de elementos, componentes y materiales que se proyectan utilizar en el sistema en su concepción original.  


###^^Proceso cronológicamente ordenado^^
>
- Idea y concepción primaria.
- Modelado 3D en Fusion360.
- Definición de materiales y componentes principales.  
- Diseño de la electrónica y de PCB para control e interface con entradas/salidas.  
- Fabricación digital de partes en LABAs de UTEC.  
- Programación en IDE de Arduino de lógica de control del sistema y protocolo MQTT.
- Preparación de partes de fabricación manual en taller.
- Integración de partes, armado del prototipo y primeras pruebas.
- Implementación de interface de visualización remota del estado del sistema (Node RED).

---
####^^Idea y concepción primaria^^
Desarrollado en página de [idea](../proyecto/idea.md)  

####^^Modelado 3D^^  
Desarrollado en [MT04](../modulos/MT04.md#desafio)  
Archivo Fusion360: [Compostasio3D.f3z](../proyecto/Compostasio3D.f3z)

####^^Materiales y componentes^^  
A esta altura se definieron los materiales principales que componen el sistema:  

- Estructura/costillas externas: Madera contrachapada 12mm ( plywood).
- Contenedor: Placas de plástico reciclado 12mm.
- Eje y paletas mezcladoras: Hierro.
- Control: ESP32
- Display local: OLED 0.96" SSD1306
- Sensores: 3 temp DS18B20 + 2 humedad capacitivos
- Driver de motor: tarjeta de 2 relés para implementar puente H (cambio giro motor).
- Motor: 12V wipe motor (limpiaparabrisas)
- Respaldo de fuente del ESP32: panel solar + driver + batería 2000mAh  

####^^Diseño de la electrónica y PCB^^  
La idea fue poder integrar en una sola placa: el montaje del ESP32, pantalla oled, borneras interface con sensores de temperatura y humedad (alimentación y señal), borneras para entrada de 3 alarmas, borneras interface con motor, borneras de entrada de alimentación de la lógica.  
![](../images/proyecto/pcb1.jpg)  

Para ello usé el programa Kicad aprendido en el [MT08](../modulos/MT08.md).  
Dibujé primero el circuito eléctrico:  
![](../images/proyecto/esquematico.jpg)
Archivo del circuito eléctrico esquemático: [CompostasioV0.5.sch](../proyecto/CompostasioV0.5.kicad_sch)  

Para luego pasar al diseño del impreso a partir de los componentes definidos:  
![](../images/proyecto/pcb2.jpg)  
Archivo del circuito impreso: [CompostasioV0.5.pcb](../proyecto/CompostasioV0.5.kicad_pcb)  

####^^Fabricación digital^^
El impreso se fabricó con la máquina Mipec que se encuentra en el LABA Utec de Fray Bentos.  
==Gracias Mateo por recibirme y asistirme!!==  
![](../images/proyecto/mipec.jpg)  
![](../images/proyecto/impreso2.jpg)  

Por otra parte, en el LABA de Durazno se fabricaron con corte Láser las costillas que forman la estructura, le da forma y sostiene a las placas de plástico del compostador. También se cortaron en la láser las platinas del eje que sostienen las paletas que hacen la mezcla/avance del material en descomposición.  
==Gracias Maxi por recibirme y asistirme!!==  
![](../images/proyecto/costilla.jpg)  
![](../images/proyecto/costillaplatina.jpg)  
![](../images/proyecto/tapa.jpg)  
![](../images/proyecto/costilla2.jpg)  
![](../images/proyecto/laser2.jpg)


Archivos (dxf):  
[Costilla+tapa](../proyecto/costilla_tapa.dxf)  
[Costilla+platinas](../proyecto/costilla_platina.dxf)  
[Tapa](../proyecto/tapa.dxf)  


####^^Programación^^  
La tarjeta ESP32 nos da la posibilidad de muchas entradas/salidas, además de conectividad WiFi.  
La programación la hice en el IDE de Arduino ya que me resultaba más conocido.  
Puntos básicos del programa implementados hasta ahora:
<font size="2">
>  
- Mostrar datos en pantalla OLED.
- Conexión a red WiFi.  
- Conexión a broquer MQTT para publicar estados de sensores (HiveMQ.com).  
- Lectura de sensores.  
- Alarmas de puerta abierta, nueva carga y Stop general.  
- Giro de motor en ambos sentidos (cada cierto intervalo).  

</font>  
El código que sigue en desarrollo está así:  

    // Compostasio V0.1
    //  MQTT broker:  broker.hivemq.com

    // ***** sensores DS18B20 ******    (direcciones obtenidas con: DS18B20.ino)
    // T1: ROM = 28 9D 4B 94 97 8 3 55
    // T2: ROM = 28 F3 3 94 97 8 3 52
    // T3: ROM = 28 81 21 94 97 12 3 D4


    // ******* WIFI & MQTT ************
    #include <WiFi.h>
    #include <MQTT.h>   //// by Joël Gähwiler https://github.com/256dpi/arduino-mqtt
    #include <NTPClient.h> // timestamp https://github.com/taranais/NTPClient

    WiFiUDP ntpUDP;
    NTPClient timeClient(ntpUDP);

    const char ssid[] = "kikori-fondo";
    const char pass[] = "frismare3+2";

    // MQTT client
    WiFiClient net;
    MQTTClient client;
    //MQTTClient.setCredentials( "compostasio2", "Efdia2022");

    // ******* OLED ************
    #include <Wire.h>
    #include <Adafruit_SSD1306.h>
    #define SCREEN_WIDTH 128
    #define SCREEN_HEIGHT 64
    Adafruit_SSD1306 oled(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

    //****** Sensores DS18B20 *****
    #include <OneWire.h>
    #include <DallasTemperature.h>
    #define TEMP_BUS 4    //bus sensores temperatuta GPIO 4
    // Setup a oneWire instance to communicate with a OneWire device
    OneWire oneWire(TEMP_BUS);
    // Pass our oneWire reference to Dallas Temperature sensor
    DallasTemperature sensors(&oneWire);

    DeviceAddress sensor1 = { 0x28, 0x9D, 0x4B, 0x94, 0x97, 0x8, 0x3, 0x55 }; // obtenida con DS18B20.ino
    DeviceAddress sensor2 = { 0x28, 0xF3, 0x3, 0x94, 0x97, 0x8, 0x3, 0x52 };
    DeviceAddress sensor3 = { 0x28, 0x81, 0x21, 0x94, 0x97, 0x12, 0x3, 0xD4 };


    // ******** PINES ES32 ********
    #define ONBOARD_LED  2
    #define HUM1 32   //sensor humedad 1
    #define HUM2 33   //sensor humedad 2
    #define pin_door 27   //alarma puerta abierta
    #define pin_mix 14    //botón nueva carga
    #define pin_off 13    //llave stop general, (como interrupción)
    #define MOT1 25   //salida 1 motor
    #define MOT2 26   //salida 2 motor

    // ******** Variables SENSORES ********
    float t1 = 0;
    float t2 = 0;
    float t3 = 0;
    float h1 = 0;
    float h2 = 0;

    // ****** Rango sensores humedad ****** (rango obtenido con: calhum.ino)
    const int humAire = 3020;   //valor máximo del sensor de humedad al aire
    const int humAgua = 1630;  //valor del sensor de humedad en agua


    // ******* Estado Alarmas ******

    bool door = 0;
    bool mix = 0;
    bool off = 0;
    bool flag_off = true;
    bool lflag_off = true;
    bool lflag_mix = false;
    bool lflag_door = false;

    //unsigned long t_nowm = 0;
    String timestamp;
    //long unsigned int timestamp = 0;
    unsigned long t_lastsensor = 0; //tiempo última lectura
    const long t_leesensor = 3000; //3600000; //7200000; //lee y publica sensores cada 1 horas
    unsigned long t_lastmotor = 0; //tiempo de último movimiento motor
    unsigned long t_lastmotf = 0;
    unsigned long t_lastmotr = 0;
    unsigned long timing = 0; //cambio de giro
    const long t_forwrev = 3000; // espera entre cambio de giro
    const long t_motor = 14400000; //mueve motor cada 4 horas
    const long t_motmezf = 2000; //mueve motor nueva mezcla forward 20s
    const long t_motmezr = 5000; //mueve motor nueva mezcla reverse 20s
    //variables para el rebote del stop
    unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
    unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers


    void IRAM_ATTR sysoff() {   //función para la interrupción del stop

    if ((millis() - lastDebounceTime) > debounceDelay && flag_off == false) {
    lastDebounceTime = millis();
    out();
    //Serial.println("stop disparado");

    }
    }


    void setup() {

    Serial.begin(115200);
    WiFi.begin(ssid, pass);

    client.begin("broker.hivemq.com", net);
    client.onMessage(messageReceived);

    pinMode(ONBOARD_LED, OUTPUT);

    sensors.begin();    // temperatura

    digitalWrite(MOT1, HIGH); //motor apagado al iniciar
    pinMode(MOT1, OUTPUT);
    digitalWrite(MOT2, HIGH);
    pinMode(MOT2, OUTPUT);

    // initialize OLED display with I2C address 0x3C
    if (!oled.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    //Serial.println(F("failed to start SSD1306 OLED"));
    while (1);
    }
    pinMode(pin_door, INPUT_PULLUP);
    pinMode(pin_mix, INPUT_PULLUP);
    pinMode(pin_off, INPUT_PULLUP);

    attachInterrupt(pin_off, sysoff, FALLING); //stop switch dispara interrupción

    delay(2000);                        // wait two seconds for initializing
    oled.clearDisplay();                // clear display
    oled.setTextSize(1);                // set text size
    oled.setTextColor(WHITE, BLACK);   // set text color evita transparencia
    oled.setCursor(18, 0);             // set position to display
    oled.println("COMPOSTASIO"); // set text
    oled.setCursor(0, 16);
    oled.println("T1:");
    oled.setCursor(48, 16);
    oled.println("C");
    oled.setCursor(0, 26);
    oled.println("T2:");
    oled.setCursor(48, 26);
    oled.println("C");
    oled.setCursor(0, 36);
    oled.println("T3:");
    oled.setCursor(48, 36);
    oled.println("C");
    oled.setCursor(62, 16);
    oled.println("H1:");
    oled.setCursor(110, 16);
    oled.println("%");
    oled.setCursor(62, 26);
    oled.println("H2:");
    oled.setCursor(110, 26);
    oled.println("%");
    oled.display(); // display on OLED

    timeClient.begin();
    timeClient.setTimeOffset(-10800); // GMT -3

    connect();

    }


    void connect() {
    Serial.print("checking wifi...");
    if (WiFi.status() != WL_CONNECTED) {
    oled.setTextSize(1);
    oled.setTextColor(WHITE, BLACK);
    oled.setCursor(100, 0);
    oled.println("   ");
    oled.display();
    Serial.print(".");
    // delay(1000);
    }
    else if (WiFi.status() == WL_CONNECTED) {
    oled.setTextSize(1);
    oled.setCursor(100, 0);             // set position to display
    oled.println("WiFi");             // set text
    oled.display();

    }
    Serial.print("\nconnecting...");
    if (!client.connect("compostcasa", "public", "public")) {
    digitalWrite(ONBOARD_LED, HIGH);
    Serial.print(".");
    //  delay(1000);
    }
    else if (client.connected()) {
    digitalWrite(ONBOARD_LED, LOW);

    Serial.println("\nMQTT connectado!");


    client.subscribe("/control/move");
    }
    }


    //********** mqtt recibidos ***********
    void messageReceived(String &topic, String &payload) {
    Serial.println("incoming: " + topic + " - " + payload);
    if (topic == "/control/move") {

    if (payload == "on") {
      // Serial.println("on");
      newmix();
    }
    }
    }

    //********* mqtt publicados **********
    void pubsensor() {

    client.publish("/sensor/temp1", String(t1, 1));
    client.publish("/sensor/temp2", String(t2, 1));
    client.publish("/sensor/temp3", String(t3, 1));
    client.publish("/sensor/hum1", String(h1, 1));
    client.publish("/sensor/hum2", String(h2, 1));

    timeClient.update();
    timestamp = timeClient.getFormattedDate();
    // Serial.println(timestamp);
    client.publish("/sensor/time", timestamp);
    }

    void puboff() {
    if (flag_off == true && flag_off == !lflag_off) {
    client.publish("/alarm/off", "Sistema OFF");
    }
    else if (flag_off == !lflag_off) {
    client.publish("/alarm/off", "Sistema ON");
    }
    lflag_off = flag_off;
    }

    void pubmix() {

    if (mix == LOW && mix == !lflag_mix) {
    client.publish("/alarm/mix", "Nueva Carga ON");
    }
    else if (mix == HIGH && mix == !lflag_mix) {
    client.publish("/alarm/mix", "Nueva Carga OFF");
    }
    lflag_mix = mix;
    }

    void pubdoor() {
    if (door == LOW && door == !lflag_door) {
    client.publish("/alarm/door", "Puerta Abierta");
    }
    else if (door == HIGH && door == !lflag_door) {
    client.publish("/alarm/door", "Puerta Cerrada");
    }
    lflag_door = door;
    }


    void loop() {

    client.loop();
    delay(10);  // <- fixes some issues with WiFi stability
    if (!client.connected()) {
    connect();
    }

    if (millis() - t_lastsensor >= t_leesensor) {
    temperatura();    //actualiza temperatura
    humedad();        //actualiza humedad
    oled_sensor();
    pubsensor();
    }
    //****** alarmas *******
    door = digitalRead(pin_door);
    mix = digitalRead(pin_mix);
    off = digitalRead(pin_off);
    if (off == HIGH) {
    flag_off = false;
    }
    puboff();
    pubmix();
    pubdoor();

    alarmas();

    }

    //******* actualiza oled sensores ********
    void oled_sensor() {

    oled.setTextSize(1);
    oled.setTextColor(WHITE, BLACK);
    oled.setCursor(20, 16);
    oled.println(t1, 1);

    oled.setCursor(20, 26);
    oled.println(t2, 1);

    oled.setCursor(20, 36);
    oled.println(t3, 1);

    oled.setCursor(82, 16);
    oled.println(h1, 1);

    oled.setCursor(82, 26);
    oled.println(h2, 1);

    oled.display();
    }

    //******* alarmas **********
    void alarmas() {
    if (off == LOW) {
    oled.setTextSize(2);
    oled.setCursor(0, 50);
    oled.println("STOP");
    oled.display();
    }
    off = digitalRead(pin_off);
    if (off == HIGH) {
    oled.setTextSize(2);
    oled.setTextColor(WHITE, BLACK);
    oled.setCursor(0, 50);
    oled.println("    ");
    oled.display();
    }

    if (door == LOW) {
    oled.setTextSize(2);
    oled.setCursor(48, 50);
    oled.println("DOOR");
    oled.display();
    }
    door = digitalRead(pin_door);
    if (door == HIGH) {
    oled.setTextSize(2);
    oled.setTextColor(WHITE, BLACK);
    oled.setCursor(48, 50);
    oled.println("    ");
    oled.display();
    }

    if (mix == LOW) {
    oled.setTextSize(2);
    oled.setCursor(92, 50);
    oled.println("MIX");
    oled.display();
    newmix();    //función nueva mezcla
    mix = digitalRead(pin_mix);
    if (mix == HIGH) {
      oled.setTextSize(2);
      oled.setTextColor(WHITE, BLACK);
      oled.setCursor(90, 50);
      oled.println("   ");
      oled.display();
    }
    }
    }

    //lee sensores y actualiza variables t1, t2, t3
    void temperatura() {
    sensors.requestTemperatures(); // Send the command to get temperatures
    t1 = sensors.getTempC(sensor1);
    t2 = sensors.getTempC(sensor2);
    t3 = sensors.getTempC(sensor3);
    }

    //lee sensores y actualiza variables h1, h2
    void humedad() {
    h1 = map (analogRead(HUM1), humAgua, humAire, 100, 40);
    h2 = map (analogRead(HUM2), humAgua, humAire, 100, 40);
    //Serial.println(analogRead(HUM1));
    //Serial.print(h1);
    //Serial.print("% humedad");

    t_lastsensor = millis();
    }

    void newmix() {

    if (flag_off == false) {

    digitalWrite(MOT1, LOW);
    t_lastmotf = millis();
    forward:
    if (millis() - t_lastmotf >= t_motmezf || flag_off == true) {
      if (flag_off == true) {
        digitalWrite(MOT1, HIGH);
        return;
      }
      digitalWrite(MOT1, HIGH);
      timing = millis();
      goto entregiro;
    }
    goto forward;
    }

    entregiro:
    if (millis() - timing > t_forwrev) {     // espera 3 segundos entre cambio de giro
    goto reverse;
    }
    goto entregiro;

    reverse:
    if (flag_off == false) {

    digitalWrite(MOT2, LOW);
    t_lastmotr = millis();
    m_on2:
    if (millis() - t_lastmotr >= t_motmezr || flag_off == true) {
      digitalWrite(MOT2, HIGH);
      lflag_mix = false;
      return;
    }
    goto m_on2;

    }
    }

    void out() {
    flag_off = true;

    }  

Archivo [compostasio0_1.ino](../proyecto/compostasio0_1.ino)  

####^^Fabricación manual en taller^^  

Por la naturaleza propia del prototipo a fabricar hubieron partes como el montaje del motor, la transmisión de la rotación al eje, las paletas de hierro, los soportes para los sensores, etc que implicaron trabajo manual y con máquinas habituales de taller (amoladora, taladro, sierra, herramientas eléctricas y manuales).  
Traté de registrar en imágenes lo máximo posible del trabajo y aquí las dejo:  
![](../images/proyecto/ultimas/manual1.jpg)  
![](../images/proyecto/ultimas/manual2.jpg)  
![](../images/proyecto/ultimas/manual3.jpg)  
![](../images/proyecto/ultimas/manual4.jpg)  
![](../images/proyecto/ultimas/manual5.jpg)  
![](../images/proyecto/ultimas/manual6.jpg)  
![](../images/proyecto/ultimas/manual7.jpg)  
![](../images/proyecto/ultimas/manual8.jpg)  
![](../images/proyecto/ultimas/manual9.jpg)  
![](../images/proyecto/ultimas/manual10.jpg)  
![](../images/proyecto/ultimas/manual105.jpg)
![](../images/proyecto/ultimas/manual106.jpg)    
![](../images/proyecto/ultimas/manual11.jpg)  
![](../images/proyecto/ultimas/manual102.jpg)    
![](../images/proyecto/ultimas/manual12.jpg)  
![](../images/proyecto/ultimas/manual13.jpg)  
![](../images/proyecto/ultimas/manual14.jpg)  
![](../images/proyecto/ultimas/manual16.jpg)  
![](../images/proyecto/ultimas/manual17.jpg)  
![](../images/proyecto/ultimas/manual18.jpg)  
![](../images/proyecto/ultimas/manual19.jpg)  
![](../images/proyecto/ultimas/manual20.jpg)
![](../images/proyecto/ultimas/manual201.jpg)  
![](../images/proyecto/ultimas/manual21.jpg)  
![](../images/proyecto/ultimas/manual215.jpg)
![](../images/proyecto/ultimas/manual220.jpg)
![](../images/proyecto/ultimas/manual22.jpg)  
![](../images/proyecto/ultimas/manual23.jpg)
![](../images/proyecto/ultimas/manual26.jpg)  

####^^Probando Node RED^^  

A modo de experimentación y con el objetivo de poder tener una visualización remota para los estados de Compostasio estuve aprendiendo un poco de la plataforma [Node RED](https://nodered.org/) que permite crear una interface web para visualizar los eventos que llegan por protocolo MQTT implementado en el ESP32...las primeras pruebas dejaron como resultado algo que se ve así:  
![](../images/proyecto/nodered1.jpg)
![](../images/proyecto/nodered2.jpg)  

La exploración seguirá porque me gustó bastante el tema!!!  

---  
==FINAL por ahora de esta etapa del proyecto==  
