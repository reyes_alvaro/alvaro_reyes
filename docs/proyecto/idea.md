![](../images/MP01/moon2.jpg)

###^^Contexto personal^^

![](../images/proyecto/olimp.jpg){align="right"}  Hace unos años, me tocó como docente de un espacio maker presentarme junto a 2 alumnos de secundaria del colegio [San Ignacio](https://www.sanignacio.edu.uy/) a una olimpíada de robótica del [Plan Ceibal](https://www.ceibal.edu.uy/es). Con esa excusa, abordamos el tema de la gestión de residuos orgánicos en el centro educativo y nuestra propuesta para el evento fue construir un pequeño prototipo de _compostera inteligente_ (madera, cartón, motor de impresora, arduino). <br clear="right"/>
![](../images/proyecto/compostencasa.jpg){align="left"}A partir de esa experiencia se despertó en mi un interés particular sobre el proceso de hacer compost y su impacto positivo sobre el ambiente. Comencé a compostar los residuos orgánicos de mi hogar de forma manual( pila en caja de madera) mientras continuaba leyendo artículos, visitando blogs e interesándome en temas ambientales.<br clear="left"/>  

Así que cuando comenzamos esta especialización, se disparó naturalmente la posibilidad de poder desarrollar como proyecto final un ==**compostador**==( mediana escala y bajo costo) realizable y reproducible por _**cualquiera y en cualquier lugar**_ mediante fabricación digital.  
_**`"Shoot for the moon..."`**_ Con mi proyecto final busco ser parte de ese movimiento contra el [cambio climático](https://www.un.org/es/climatechange/what-is-climate-change), contribuyendo humildemente con comunidades que busquen una alternativa cierta para **reutilizar** sus recursos orgánicos y minimizar su huella ecológica.  

Podría seguir argumentando con palabras pero propongo un alto en el camino y disfrutar escuchando al duo español **[TéCanela](https://tecanela.com/)** que expresa con música, arte y buen ritmo la imperiosa necesidad de tomar conciencia y actuar: <font size="2">**_"...que grita que grita la Tierra y la oímos todos..."_**</font>

<iframe width="560" height="315" src="https://www.youtube.com/embed/LiF2bsKko7c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

###^^Contexto global^^

Existen hoy varias señales de alarma, pero también por suerte, una creciente toma de conciencia e intentos a varios niveles de dar respuestas y revertir el daño ecológico:
<font size="2">

   - **Personas y organizaciones** que se unen para reciclar, reutilizar, minimizar plásticos de un solo uso, limpiar playas y más [(ejemplo)](https://www.montevideo.com.uy/Ciencia-y-Tecnologia/Uruguay-se-suma-al-Dia-Internacional-de-Limpieza-de-Costas-este-sabado-uc803219).
   - **Centros de enseñanza** que abordan y transmiten el valor del ambiente y su cuidado.
   - **Gobiernos locales** que ponen los medios para lograr objetivos de sostenibilidad [(Canelones, UY)](https://www.imcanelones.gub.uy/es/content/hogares-sustentables).
   - **Gobiernos nacionales** que buscan reformular la gestión de residuos y la economía circular [(Uruguay)](https://www.gub.uy/ministerio-ambiente/politicas-y-gestion/uruguay-circular).
     <iframe width="560" height="315" src="https://www.youtube.com/embed/J18IOW_URyU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br/><br/>
  - **Líderes mundiales** que reflexionan y acuerdan acciones para frenar el calentamiento global en el [COP26.](https://unfccc.int/conference/glasgow-climate-change-conference-october-november-2021)
    </font>  

###^^Concepto^^

Enmarcado en esta realidad es que pretendo desarrollar un _**==Compostador==** de mediana escala_ automático, de bajo costo, para utilizar in-situ y que pueda ser construido con **fabricación digital**. El proyecto será **_open source_**, para que se pueda replicar en cualquier parte del país, continente, mundo...(¿ no será mucho?:smile:).  
Producir compost( _hot compost_) es permitir la descomposición biológica de materiales orgánicos pero controlando aspectos como el **oxígeno**, la **humedad** y **temperatura**, de tal forma que dicha transformación permita la obtención de un nuevo producto orgánico estable, rico en nutrientes y carbono, muy valioso para la regeneración y mantenimiento de los suelos.  
Paralelamente, la producción de compost implica la gestión de residuos, o mejor dicho, **_recursos orgánicos_**, que no terminarán en vertederos donde la descomposición sin oxígeno provoca la emisión de metano ( poderoso gas de efecto invernadero).[^1]  
Pero, producir compost a mediana escala de forma efectiva y en poco tiempo **no** es una tarea sencilla. Requiere atención y control de los diversos aspectos involucrados. Así es que muchas iniciativas no perduran o son abandonadas por falta de mantenimiento.  
Existen métodos a baja escala para hacer compost en los hogares y también existen soluciones a nivel industrial para procesar grandes cantidades de material orgánico. Pero no son comunes las soluciones eficientes, sostenibles y a relativo bajo costo destinada a pequeñas comunidades (colegios, universidades, centros sociales, complejos de viviendas, etc.).  
Así nace: ==COMPOSTASIO== &#8594;(Compost A~utomático~ S~ostenible~ I~n-situ~ O~pen-source~ )    

[^1]: Uno de los compromisos de la [COP26](https://ukcop26.org/world-leaders-kick-start-accelerated-climate-action-at-cop26/) es la reducción de emisiones de metáno.

!!! tip "Extra!"
    Durante la búsqueda de material me encontré con este video muy ilustrativo de la organización [Kiss the Ground](https://kisstheground.com/) sobre el valor y "poder" del Compost...Vale la pena verlo!
    <iframe width="560" height="315" src="https://www.youtube.com/embed/bqDQD8cvO5Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  


###^^Infografía de la idea^^
Primer esquema de la idea preliminar presentada y ampliada en el módulo [MP01](../modulos/MP01.md):

![](../images/proyecto/idea.jpg)
