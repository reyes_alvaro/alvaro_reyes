// Compostasio V0.1

//  MQTT broker:  broker.hivemq.com

// ***** sensores DS18B20 ******    (direcciones obtenidas con: DS18B20.ino)
// T1: ROM = 28 9D 4B 94 97 8 3 55
// T2: ROM = 28 F3 3 94 97 8 3 52
// T3: ROM = 28 81 21 94 97 12 3 D4


// ******* WIFI & MQTT ************
#include <WiFi.h>
#include <MQTT.h>   //// by Joël Gähwiler https://github.com/256dpi/arduino-mqtt
#include <NTPClient.h> // timestamp https://github.com/taranais/NTPClient

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

const char ssid[] = "kikori-fondo";
const char pass[] = "frismare3+2";

// MQTT client
WiFiClient net;
MQTTClient client;
//MQTTClient.setCredentials( "compostasio2", "Efdia2022");

// ******* OLED ************
#include <Wire.h>
#include <Adafruit_SSD1306.h>
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
Adafruit_SSD1306 oled(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

//****** Sensores DS18B20 *****
#include <OneWire.h>
#include <DallasTemperature.h>
#define TEMP_BUS 4    //bus sensores temperatuta GPIO 4
// Setup a oneWire instance to communicate with a OneWire device
OneWire oneWire(TEMP_BUS);
// Pass our oneWire reference to Dallas Temperature sensor
DallasTemperature sensors(&oneWire);

DeviceAddress sensor1 = { 0x28, 0x9D, 0x4B, 0x94, 0x97, 0x8, 0x3, 0x55 }; // obtenida con DS18B20.ino
DeviceAddress sensor2 = { 0x28, 0xF3, 0x3, 0x94, 0x97, 0x8, 0x3, 0x52 };
DeviceAddress sensor3 = { 0x28, 0x81, 0x21, 0x94, 0x97, 0x12, 0x3, 0xD4 };


// ******** PINES ES32 ********
#define ONBOARD_LED  2
#define HUM1 32   //sensor humedad 1
#define HUM2 33   //sensor humedad 2
#define pin_door 27   //alarma puerta abierta
#define pin_mix 14    //botón nueva carga
#define pin_off 13    //llave stop general, (como interrupción)
#define MOT1 25   //salida 1 motor
#define MOT2 26   //salida 2 motor

// ******** Variables SENSORES ********
float t1 = 0;
float t2 = 0;
float t3 = 0;
float h1 = 0;
float h2 = 0;

// ****** Rango sensores humedad ****** (rango obtenido con: calhum.ino)
const int humAire = 3020;   //valor máximo del sensor de humedad al aire
const int humAgua = 1630;  //valor del sensor de humedad en agua


// ******* Estado Alarmas ******

bool door = 0;
bool mix = 0;
bool off = 0;
bool flag_off = true;
bool lflag_off = true;
bool lflag_mix = false;
bool lflag_door = false;

//unsigned long t_nowm = 0;
String timestamp;
//long unsigned int timestamp = 0;
unsigned long t_lastsensor = 0; //tiempo última lectura
const long t_leesensor = 3000; //3600000; //7200000; //lee y publica sensores cada 1 horas
unsigned long t_lastmotor = 0; //tiempo de último movimiento motor
unsigned long t_lastmotf = 0;
unsigned long t_lastmotr = 0;
unsigned long timing = 0; //cambio de giro
const long t_forwrev = 3000; // espera entre cambio de giro
const long t_motor = 14400000; //mueve motor cada 4 horas
const long t_motmezf = 2000; //mueve motor nueva mezcla forward 20s
const long t_motmezr = 5000; //mueve motor nueva mezcla reverse 20s
//variables para el rebote del stop
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers


void IRAM_ATTR sysoff() {   //función para la interrupción del stop

  if ((millis() - lastDebounceTime) > debounceDelay && flag_off == false) {
    lastDebounceTime = millis();
    out();
    //Serial.println("stop disparado");

  }
}


void setup() {

  Serial.begin(115200);
  WiFi.begin(ssid, pass);

  client.begin("broker.hivemq.com", net);
  client.onMessage(messageReceived);

  pinMode(ONBOARD_LED, OUTPUT);

  sensors.begin();    // temperatura

  digitalWrite(MOT1, HIGH); //motor apagado al iniciar
  pinMode(MOT1, OUTPUT);
  digitalWrite(MOT2, HIGH);
  pinMode(MOT2, OUTPUT);

  // initialize OLED display with I2C address 0x3C
  if (!oled.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    //Serial.println(F("failed to start SSD1306 OLED"));
    while (1);
  }
  pinMode(pin_door, INPUT_PULLUP);
  pinMode(pin_mix, INPUT_PULLUP);
  pinMode(pin_off, INPUT_PULLUP);

  attachInterrupt(pin_off, sysoff, FALLING); //stop switch dispara interrupción

  delay(2000);                        // wait two seconds for initializing
  oled.clearDisplay();                // clear display
  oled.setTextSize(1);                // set text size
  oled.setTextColor(WHITE, BLACK);   // set text color evita transparencia
  oled.setCursor(18, 0);             // set position to display
  oled.println("COMPOSTASIO"); // set text
  oled.setCursor(0, 16);
  oled.println("T1:");
  oled.setCursor(48, 16);
  oled.println("C");
  oled.setCursor(0, 26);
  oled.println("T2:");
  oled.setCursor(48, 26);
  oled.println("C");
  oled.setCursor(0, 36);
  oled.println("T3:");
  oled.setCursor(48, 36);
  oled.println("C");
  oled.setCursor(62, 16);
  oled.println("H1:");
  oled.setCursor(110, 16);
  oled.println("%");
  oled.setCursor(62, 26);
  oled.println("H2:");
  oled.setCursor(110, 26);
  oled.println("%");
  oled.display(); // display on OLED

  timeClient.begin();
  timeClient.setTimeOffset(-10800); // GMT -3

  connect();

}


void connect() {
  Serial.print("checking wifi...");
  if (WiFi.status() != WL_CONNECTED) {
    oled.setTextSize(1);
    oled.setTextColor(WHITE, BLACK);
    oled.setCursor(100, 0);
    oled.println("   ");
    oled.display();
    Serial.print(".");
    // delay(1000);
  }
  else if (WiFi.status() == WL_CONNECTED) {
    oled.setTextSize(1);
    oled.setCursor(100, 0);             // set position to display
    oled.println("WiFi");             // set text
    oled.display();

  }
  Serial.print("\nconnecting...");
  if (!client.connect("compostcasa", "public", "public")) {
    digitalWrite(ONBOARD_LED, HIGH);
    Serial.print(".");
    //  delay(1000);
  }
  else if (client.connected()) {
    digitalWrite(ONBOARD_LED, LOW);

    Serial.println("\nMQTT connectado!");


    client.subscribe("/control/move");
  }
}


//********** mqtt recibidos ***********
void messageReceived(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
  if (topic == "/control/move") {

    if (payload == "on") {
      // Serial.println("on");
      newmix();
    }
  }
}

//********* mqtt publicados **********
void pubsensor() {

  client.publish("/sensor/temp1", String(t1, 1));
  client.publish("/sensor/temp2", String(t2, 1));
  client.publish("/sensor/temp3", String(t3, 1));
  client.publish("/sensor/hum1", String(h1, 1));
  client.publish("/sensor/hum2", String(h2, 1));

  timeClient.update();
  timestamp = timeClient.getFormattedDate();
  // Serial.println(timestamp);
  client.publish("/sensor/time", timestamp);
}

void puboff() {
  if (flag_off == true && flag_off == !lflag_off) {
    client.publish("/alarm/off", "Sistema OFF");
  }
  else if (flag_off == !lflag_off) {
    client.publish("/alarm/off", "Sistema ON");
  }
  lflag_off = flag_off;
}

void pubmix() {

  if (mix == LOW && mix == !lflag_mix) {
    client.publish("/alarm/mix", "Nueva Carga ON");
  }
  else if (mix == HIGH && mix == !lflag_mix) {
    client.publish("/alarm/mix", "Nueva Carga OFF");
  }
  lflag_mix = mix;
}

void pubdoor() {
  if (door == LOW && door == !lflag_door) {
    client.publish("/alarm/door", "Puerta Abierta");
  }
  else if (door == HIGH && door == !lflag_door) {
    client.publish("/alarm/door", "Puerta Cerrada");
  }
  lflag_door = door;
}


void loop() {

  client.loop();
  delay(10);  // <- fixes some issues with WiFi stability
  if (!client.connected()) {
    connect();
  }

  if (millis() - t_lastsensor >= t_leesensor) {
    temperatura();    //actualiza temperatura
    humedad();        //actualiza humedad
    oled_sensor();
    pubsensor();
  }
  //****** alarmas *******
  door = digitalRead(pin_door);
  mix = digitalRead(pin_mix);
  off = digitalRead(pin_off);
  if (off == HIGH) {
    flag_off = false;
  }
  puboff();
  pubmix();
  pubdoor();

  alarmas();

}

//******* actualiza oled sensores ********
void oled_sensor() {

  oled.setTextSize(1);
  oled.setTextColor(WHITE, BLACK);
  oled.setCursor(20, 16);
  oled.println(t1, 1);

  oled.setCursor(20, 26);
  oled.println(t2, 1);

  oled.setCursor(20, 36);
  oled.println(t3, 1);

  oled.setCursor(82, 16);
  oled.println(h1, 1);

  oled.setCursor(82, 26);
  oled.println(h2, 1);

  oled.display();
}

//******* alarmas **********
void alarmas() {
  if (off == LOW) {
    oled.setTextSize(2);
    oled.setCursor(0, 50);
    oled.println("STOP");
    oled.display();
  }
  off = digitalRead(pin_off);
  if (off == HIGH) {
    oled.setTextSize(2);
    oled.setTextColor(WHITE, BLACK);
    oled.setCursor(0, 50);
    oled.println("    ");
    oled.display();
  }

  if (door == LOW) {
    oled.setTextSize(2);
    oled.setCursor(48, 50);
    oled.println("DOOR");
    oled.display();
  }
  door = digitalRead(pin_door);
  if (door == HIGH) {
    oled.setTextSize(2);
    oled.setTextColor(WHITE, BLACK);
    oled.setCursor(48, 50);
    oled.println("    ");
    oled.display();
  }

  if (mix == LOW) {
    oled.setTextSize(2);
    oled.setCursor(92, 50);
    oled.println("MIX");
    oled.display();
    newmix();    //función nueva mezcla
    mix = digitalRead(pin_mix);
    if (mix == HIGH) {
      oled.setTextSize(2);
      oled.setTextColor(WHITE, BLACK);
      oled.setCursor(90, 50);
      oled.println("   ");
      oled.display();
    }
  }
}

//lee sensores y actualiza variables t1, t2, t3
void temperatura() {
  sensors.requestTemperatures(); // Send the command to get temperatures
  t1 = sensors.getTempC(sensor1);
  t2 = sensors.getTempC(sensor2);
  t3 = sensors.getTempC(sensor3);
}

//lee sensores y actualiza variables h1, h2
void humedad() {
  h1 = map (analogRead(HUM1), humAgua, humAire, 100, 40);
  h2 = map (analogRead(HUM2), humAgua, humAire, 100, 40);
  //Serial.println(analogRead(HUM1));
  //Serial.print(h1);
  //Serial.print("% humedad");

  t_lastsensor = millis();
}

void newmix() {

  if (flag_off == false) {

    digitalWrite(MOT1, LOW);
    t_lastmotf = millis();
forward:
    if (millis() - t_lastmotf >= t_motmezf || flag_off == true) {
      if (flag_off == true) {
        digitalWrite(MOT1, HIGH);
        return;
      }
      digitalWrite(MOT1, HIGH);
      timing = millis();
      goto entregiro;
    }
    goto forward;
  }

entregiro:
  if (millis() - timing > t_forwrev) {     // espera 3 segundos entre cambio de giro
    goto reverse;
  }
  goto entregiro;

reverse:
  if (flag_off == false) {

    digitalWrite(MOT2, LOW);
    t_lastmotr = millis();
m_on2:
    if (millis() - t_lastmotr >= t_motmezr || flag_off == true) {
      digitalWrite(MOT2, HIGH);
      lflag_mix = false;
      return;
    }
    goto m_on2;

  }
}

void out() {
  flag_off = true;

}
