En esta sección iré contando los puntos que surgen en relación al proyecto final a lo largo del curso en sus diferentes tópicos, módulos, y desafíos. Los progresos, búsquedas y pruebas. Los fallos, las posibles mejoras y conclusiones en cada etapa.  

!!! Tip "Vale la pena mirar esto!"
    Project Development by Neil Gershefeld Director of MIT’s Center for Bits and Atoms.
    [![](../images/proyecto/neil.jpg)](https://vimeo.com/558297880)  

Ahora siguiendo los consejos de quienes más saben voy a tener una tabla que irá mostrando el estado de desarrollo hasta la fecha: <font size="2"> 17/11/2021. </font>

| Tareas hechas | Tareas esenciales por hacer| Tareas de futuro|
|-------------------------------------|--------------------|---------------|
| Elegir el proyecto      | Prototipo 3D en Fusion360   | Implementar ampliación por módulos      |
| Definir módulos esenciales| Definir motor ppal. y transmisión     | Implementar energía solar( electrónica) |
| Definir dimensiones generales| Definir electrónica: Humedad, Temp., controlador(Arduino)| |Definir fuentes energía sistema                                             |                                         |
| Prototipo caja recolector orgánicos |                                                             |                                         |





- ### ^^Isotipo/Logo^^

El desafío para trabajar con el software CAD fue crear un isotipo para identificar mi proyecto y así surgió la primera versión de un posible _logo_. Proceso y archivos de la primer versión en el [MT02.](../modulos/MT02.md#proceso-vector). Con el tiempo van surgiendo alternativas de colores y formas para ir probando.

![](../images/MT02/isotipoF.png){width="300"}
![](../images/proyecto/isotipo2.png){width="300"}
![](../images/proyecto/etiqueta.png){width=""}
![](../images/proyecto/etiqueta3.png){width=""}
![](../images/proyecto/etiqueta2.png){width=""}


- ### ^^Cómo recolectamos^^?

 El desafío era experimentar con el _corte laser_ y con ese fin diseñé un prototipo de envase para recibir el material orgánico que serán los recursos para nuestro compostador. La idea es distribuir estos contenedores en diferentes puntos clave cercanos a los usuarios para que el sistema de recolección sea efectivo. Proceso y archivos en el [MT03.](../modulos/MT03.md#desafio)
!!! info "Nota"
    La prueba se hizo con Mdf 3mm pero lo más apropiado para la versión final creo que será usar _Plywood 3mm._
![](../images/MT03/recolector.jpg)  

- ### ^^Modelo en 3D^^

  Con el fin de ir determinando formas, medidas y aspectos constructivos básicos me resultó interesante dedicar el [MT04](../modulos/MT04.md#proyectando-y-modelando) a intentar modelar una posible aproximación a un primer prototipo del ^^Compostasio^^.  

---
  :eyes:La historia sigue [aquí](../proyecto/proyecto.md)   
