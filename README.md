## Bienvenidos a la web personal de la Especialización en Fabricación Digital e Innovación Abierta de UTEC y FabLab Barcelona.  


### **_Descripción_**  

En este repositorio se encuentra:
- el camino de aprendizaje durante la especialización
- la documentación de forma detallada del Proyecto Final: *Compostasio* (Compost Automático Sostenible In-situ Open source)

URL web: https://reyes_alvaro.gitlab.io/alvaro_reyes/
